---
layout: job_family_page
title: "People Business Partner"
---

People Business Partners (PBPs) are responsible for aligning business objectives with people solutions in the organizations they support. In many companies, the role is referred to as HRBP. Successful PBPs act as a business professionals, employee champions, and change agents. PBPs are HR experts who partner with and advise the business by maintaining literacy about the business unit's financial position, its midrange plans, its way of operating, and its competition. 

### Director, HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with executive level management, providing People guidance when appropriate.
* Mentors and coaches managers and other People Team Members.
* Identifies great talent, internally and externally, who will raise the bar on the team.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of team members, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with team members globally to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning to support the business strategy.
* Identifies learning needs for business units and individual executive coaching needs. Participates in evaluation and monitoring of learning solutions to ensure success. Follows up to ensure learning and development objectives are met.

## Requirements

* Minimum of 10 years of relevant experience, with at least 5 years of experience supporting Senior Executive and/or C-level leaders. 
* Strong attention to detail and ability to work well with fluid information.
* Comfortable using technology, especially open source technology and G suite.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members. At times, courage is required to respectfully speak up, “push back”, challenge “the way things have always been done” and even disagree with leaders and executives.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Have implemented various learning and development programs that are aligned to the business needs
* Experience working with Global Teams
* Very strong EQ, with fine-tuned instincts and problem-solving skills.
* Strong drive and initiative with the ability to self-manage.

### Senior HRBP

## Responsibilities

* Provide strategic HR support to client groups and consults with VPs, Directors and Managers in areas of performance management, management training and coaching, organizational development, and employee relations.
* Provides HR policy guidance and interpretation.
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Work closely with HR colleagues in implementing organization wide initiatives.
* Analyze trends and metrics in partnership with HR group to develop solutions, programs and policies.
* Raise concerns or trends to the CCO, legal, and/or HRBP directors to address timely and effectively.
* Being an enthusiastic team player with a strong drive to create a positive work environment.
* The ability to be comfortable with high volume workload and not be afraid to "roll up your sleeves".
* Experience working with Global teams. 


## Requirements

* 5 years of relvant HR experience supporting front line, mid and senior-level leaders.
* Strong attention to detail and ability to work well with fluid information
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Very strong EQ, with fine-tuned instincts and problem-solving skills.

### Intermediate HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with line management, providing People guidance when appropriate.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of employees, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with colleagues outside the US to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning.
* Identifies training needs for business units and individual executive coaching needs.
* Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* 3 years of HR in recruiting, employee relations, and/or organizational development.
* Demonstrate discretion and sound judgment while working with sensitive and confidential materials.
* Comfortable using technology.
* Effective verbal and written communications.
* Passion for results.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment.
* Resourceful and takes initiative to seek internal and external resources when problem-solving.
* Ability to solve moderate to complex problems
* Experience working with Global Talent in areas like Europe, India, and China
* Very strong EQ, with fine tuned instincts and problem solving skills.

## Specialties

### People Business Partner, Engineering:
* 5-10 years relevant experience in a high growth, fast moving company where ongoing change is the norm, with at least 2-3 of those years supporting a SaaS or related software organization.
* Partner with the Engineering VP on the people strategy for their organization.
* Provide support and guidance to people leaders at all levels within the Engineering organization (front line manager to director).
* Understand GitLab’s Engineering strategy in order to align people strategies to meet business goals.
* Support the Engineering team through all People Ops processes including compensation, talent development, performance management, employee relations, change management and organizational design.
* Analytically driven, experience  in utilizing qualitative and quantitative approaches to problem solving and root cause analysis. 
* Drive equality, diversity, and inclusion throughout all of our programs and initiatives.

### People Business Partner, Sales:
* 5-10 years of relevant experience in a high growth, fast moving company where ongoing change is the norm, with at least 2-3 of those years supporting a global software sales organization with particular focus on Enterprise Sales, Commercial Sales and Customer Success. 
* Partner with 1 or more Sales VPs on the people strategy for their organization  
* Provide support and guidance to people leaders at all levels within the sales organization (front line manager to director)
* Experience with sales compensation, sales plans, sales data/analysis as needed, in partnership with the Sales Operations and People Operations team.
* Understand GitLab’s sales strategy in order to align people strategies to meet business goals.
* Drive equality, diversity, and inclusion throughout all of our programs and initiatives. 

## Performance Indicators
Performance indicators are done in partnership with the PBP and the organizational leaders.

- [12 Month Team member retention](https://about.gitlab.com/handbook/people-group/people-group-metrics/#team-member-retention)
- [12 Month voluntary team member turnover](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#team-member-turnover)
- [12 Month PIP success rate](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#regrettable-attrition)
- [Percent of team members over compensation band](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Discretionary Bonuses](https://about.gitlab.com/handbook/incentives/#discretionary-bonuses)

