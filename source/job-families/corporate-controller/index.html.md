---
layout: markdown_page
title: Director - Corporate Controller
---

The Corporate Controller leads the accounting and financial reporting activities and is responsible for the leadership, development and execution of the company’s corporate accounting function. Reporting to the Principal Accounting Officer, the Corporate Controller enhances financial controls, processes and oversees compliance with our policies and procedures. These functions and processes produce timely and accurate monthly financial results in compliance with US GAAP and SEC reporting requirements. The Controller is also responsible for capturing, defining and ensuring accurancy of non-gaap finanical metrics.

The Controller works closely with our external and internal auditors, and other members of the organization to complete our monthly and quarterly reporting on a timely basis. This role instills a culture of strong internal controls across the company and is responsible for compliance with our technical accounting standards, GL and sub-modules for efficient operations and global revenue.

## Responsibilities

1. Oversees the monthly close procees and is accountable for achieving our target for days to close.
1. Ensure consistent compliance of corporate accounting principles and procedures in full compliance with US GAAP and SEC reporting standards. 
1. Be involved in the global revenue recognition review.
1. Hands-on experience in managing accounting operations, monthly/quarterly accounting close and preparation of financial statements.
1. Ability to optimize and automate accounting processes to drive a faster monthly/quarterly close cycle.
1. Experience with Netsuite.
1. Work with the team to assess and report status of existing accounting and reporting standards, procedures and internal controls to identify areas of improvements. Contribute to remediation plans when deficiencies are noted in testing of internal controls.
1. Partner effectively with other finance team members and non-accounting functions to drive improved controls and process enhancements.
1. Ability to make decisions, prioritize effectively and meet deadlines while maintaining the highest level of standards for completeness and accuracy.
1. Enable increased level of employee job satisfaction and employee engagement through leadership, coaching and development of the team.
1. Efficiency in leading remote employees.

## Requirements

1. Proven experience leading to a high-performing accounting and control organization.
1. Leadership experience in a software or global technology company.
1. Strategic vision and best-practices to guide the team.
1. Tactical approach to enable effective implementation.
1. The individual will demonstrate strong business judgment applied to routine and complex financial and accounting issues.
1. Professional curiosity when addressing business and accounting issues.
1. Demonstrated ability to work with investor facing teams to showcase the company's financial results.
1. International experience working with overseas financial operations.
1. Over 10 years of experience dealing with complex accounting matters.
1. A CPA along with a bachelor’s degree in Accounting or equivalent is required.
1. Proven success as a Corporate Controller of complex global business entity, and ability to handle the financial accounting and financial reporting.
1. Significant technical accounting background.
1. Willing to get into the details to understand business processes at a deep level with the ability to drive efficiencies.
1. Proven ability to drive change in accounting processes driving faster monthly/quarterly close while improving employee engagement.
1. Self-motivated team player who can credibly communicate with and relate to people at all levels in the finance and accounting organization, with experience to mentor the team.
1. Demonstrated ability to work across culturally and to succeed in a dynamic environment.

## Performance Indicators

1. [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition) 
1. [Number of material audit adjustments](/handbook/internal-audit/#performance-measures-for-accounting-related-to-audit)
1. [Average days of sales outstanding](/handbook/finance/accounting/#11-accounts-receivable) 
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
1. [Percentage of ineffective SOX Controls](/handbook/internal-audit/#performance-measures-for-accounting-related-to-audit)

## Hiring Process

* Interview with PAO, CFO, direct reports, VP-Finance, Director of Revenue